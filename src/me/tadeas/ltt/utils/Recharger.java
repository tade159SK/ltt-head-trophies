package me.tadeas.ltt.utils;

import me.tadeas.ltt.HeadTrophies;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Iterator;

public class Recharger implements Listener {

    private HeadTrophies plugin;
    private HashMap<Player, HashMap<String, Long>> recharging = new HashMap<>();
    private HashMap<String, Long> maxTime = new HashMap<>();

    public Recharger(HeadTrophies plugin) {
        this.plugin = plugin;

        Bukkit.getPluginManager().registerEvents(this, this.plugin);

        new BukkitRunnable() {
            @Override
            public void run() {
                onUpdate();
            }
        }.runTaskTimerAsynchronously(plugin, 0, 5);
    }

    public void onUpdate() {
        Iterator<Player> playerIterator = recharging.keySet().iterator();
        while (playerIterator.hasNext()) {
            Player player = playerIterator.next();

            HashMap<String, Long> timer = recharging.get(player);
            if (timer.isEmpty()) {
                playerIterator.remove();
                continue;
            }

            Iterator<String> timerIterator = timer.keySet().iterator();
            while (timerIterator.hasNext()) {
                String name = timerIterator.next();
                if (!isRecharging(player, name)) {
                    timerIterator.remove();
                    continue;
                }
            }
        }
    }

    public boolean addToRecharge(Player player, long time, String name) {
        if (isRecharging(player, name)) {
            return true;
        }
        if (recharging.containsKey(player)) {
            recharging.get(player).put(name, System.currentTimeMillis() + time);
            maxTime.put(name, time);
            return false;
        }
        HashMap<String, Long> locker = new HashMap<>();
        locker.put(name, System.currentTimeMillis() + time);
        recharging.put(player, locker);
        maxTime.put(name, time);
        return false;
    }

    public boolean isRecharging(Player player, String name) {
        if (!recharging.containsKey(player))
            return false;
        HashMap<String, Long> timer = recharging.get(player);
        return timer.containsKey(name) && timer.get(name) >= System.currentTimeMillis();
    }

    public void removeAllRecharge(Player player) {
        recharging.remove(player);
    }
}
