package me.tadeas.ltt.utils;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ItemBuilder {

    private LeatherArmorMeta leatherArmorMeta;
    private Material material;
    private byte data = 0;
    private int amount = 1;
    private ItemStack finalStack;
    private String name;
    private List<String> lore;
    private ItemFlag[] flags;
    private HashMap<Enchantment, Integer> enchantments = new HashMap<>();
    private Color color;
    private SkullMeta skullMeta;

    public ItemBuilder(Material material, byte data, int amount) {
        this.material = material;
        this.data = data;
        this.amount = amount;
    }

    public ItemBuilder(Material material) {
        this.material = material;
    }

    public ItemBuilder(ItemStack finalStack) {
        this.finalStack = finalStack;

        if (finalStack.getItemMeta() instanceof LeatherArmorMeta)
            leatherArmorMeta = (LeatherArmorMeta) finalStack.getItemMeta();

        if (leatherArmorMeta != null)
            color = leatherArmorMeta.getColor();

        setMaterial(finalStack.getType());
        setAmount(finalStack.getAmount());
        setData(finalStack.getData().getData());
        setName(finalStack.getItemMeta().getDisplayName());
        setLore(finalStack.getItemMeta().getLore());
        setFlags(finalStack.getItemMeta().getItemFlags().toArray(new ItemFlag[0]));
        setEnchantments(finalStack.getItemMeta().getEnchants());
    }

    public ItemBuilder() {
    }

    public ItemBuilder setFinalStack(ItemStack finalStack) {
        this.finalStack = finalStack;
        return this;
    }

    public ItemBuilder setMaterial(Material material) {
        this.material = material;
        return this;
    }

    public ItemBuilder setData(byte data) {
        this.data = data;
        return this;
    }

    public ItemBuilder setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public ItemBuilder setFlags(ItemFlag[] flags) {
        this.flags = flags;
        return this;
    }

    public ItemBuilder setEnchantments(Enchantment enchantment, int level) {
        this.enchantments.put(enchantment, level);
        return this;
    }

    public ItemBuilder setEnchantments(Map<Enchantment, Integer> enchantments) {
        for (Enchantment enchantment : enchantments.keySet())
            this.enchantments.put(enchantment, enchantments.get(enchantment));
        return this;
    }

    public String getName() {
        return name;
    }

    public ItemBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public List<String> getLore() {
        return lore;
    }

    public ItemBuilder setLore(List<String> lore) {
        this.lore = lore;
        return this;
    }

    public ItemStack toFinalStack() {
        finalStack = new ItemStack(material, amount, data);
        ItemMeta itemMeta = finalStack.getItemMeta();
        if (leatherArmorMeta != null)
            itemMeta = leatherArmorMeta;

        if (name != null) itemMeta.setDisplayName(name);
        if (lore != null) itemMeta.setLore(lore);
        if (flags != null) itemMeta.addItemFlags(flags);
        if (enchantments != null) {
            for (Enchantment enchantment : enchantments.keySet())
                itemMeta.addEnchant(enchantment, enchantments.get(enchantment), true);
        }
        finalStack.setItemMeta(itemMeta);
        return finalStack;
    }

    public ItemStack toSkullFinalStack() {
        if (finalStack == null)
            toFinalStack();
        SkullMeta itemMeta = (SkullMeta) finalStack.getItemMeta();
        if (name != null) itemMeta.setDisplayName(name);
        if (lore != null) itemMeta.setLore(lore);
        if (flags != null) itemMeta.addItemFlags(flags);
        if (enchantments != null) {
            for (Enchantment enchantment : enchantments.keySet())
                itemMeta.addEnchant(enchantment, enchantments.get(enchantment), true);
        }
        finalStack.setItemMeta(itemMeta);
        return finalStack;
    }

    public ItemStack toSkullFinalStack(String hash) {
        SkullMeta itemMeta = (SkullMeta) toFinalStack().getItemMeta();
        itemMeta.setOwner("Notch");
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        profile.getProperties().put("textures", new Property("textures", hash));
        Field profileField;
        try {
            profileField = itemMeta.getClass().getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(itemMeta, profile);
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
            e1.printStackTrace();
        }
        finalStack.setItemMeta(itemMeta);
        return finalStack;
    }

    public ItemStack toLeatherFinalStack(Color color) {
        LeatherArmorMeta itemMeta = (LeatherArmorMeta) toFinalStack().getItemMeta();
        if (color == null)
            color = this.color;
        itemMeta.setColor(color);
        finalStack.setItemMeta(itemMeta);
        return finalStack;
    }
}
