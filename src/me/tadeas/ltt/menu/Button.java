package me.tadeas.ltt.menu;

import org.bukkit.event.inventory.InventoryClickEvent;

public interface Button {

    void onClickItem(InventoryClickEvent event);
}
