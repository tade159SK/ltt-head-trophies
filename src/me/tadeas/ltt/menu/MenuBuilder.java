package me.tadeas.ltt.menu;

import me.tadeas.ltt.HeadTrophies;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class MenuBuilder implements Listener {

    private String name;
    private int size;
    private HashMap<Integer, Button> buttons = new HashMap<>();
    private Inventory inventory;
    private HeadTrophies plugin;
    private Player player;
    private List<BukkitTask> taskList = new ArrayList<>();

    public MenuBuilder(String name, int size, HeadTrophies plugin, Player player) {
        this.name = name;
        this.size = size;
        this.plugin = plugin;
        this.player = player;

        register();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return size;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public HeadTrophies getPlugin() {
        return plugin;
    }

    public HashMap<Integer, Button> getButtons() {
        return buttons;
    }

    public Player getPlayer() {
        return player;
    }

    public List<BukkitTask> getTaskList() {
        return taskList;
    }

    public MenuBuilder getMenuBuilder() {
        return this;
    }

    public void addItem(int slot, ItemStack itemStack) {
        inventory.setItem(slot, itemStack);
    }

    public void addButtonItem(int slot, ItemStack itemStack, Button button) {
        inventory.setItem(slot, itemStack);
        buttons.put(slot, button);
    }

    public abstract void buildInventory();

    public void rebuildInventory() {
        buttons.clear();
        inventory = Bukkit.createInventory(player, size, name);

        buildInventory();

        player.openInventory(inventory);
    }

    public void register() {
        plugin.getServer().getPluginManager().registerEvents(this, getPlugin());
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();

        if (!player.equals(getPlayer()))
            return;

        if(event.getClickedInventory() == null)
            return;

        if (!event.getClickedInventory().equals(getInventory()))
            return;

        event.setResult(Event.Result.DENY);
        event.setCancelled(true);

        if (event.getCurrentItem() == null)
            return;

        if (buttons.containsKey(event.getSlot())) {
            buttons.get(event.getSlot()).onClickItem(event);
        }
    }

    @EventHandler
    public void onInventoryDrag(InventoryDragEvent event) {
        Player player = (Player) event.getWhoClicked();

        if (!player.equals(getPlayer()))
            return;

        if (!event.getInventory().equals(getInventory()))
            return;

        event.setCancelled(true);
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        if (!event.getInventory().equals(getInventory()))
            return;

        HandlerList.unregisterAll(this);

        for (BukkitTask task : getTaskList())
            task.cancel();

        getTaskList().clear();
        buttons.clear();
    }
}