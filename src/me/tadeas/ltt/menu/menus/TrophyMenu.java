package me.tadeas.ltt.menu.menus;

import me.tadeas.ltt.HeadTrophies;
import me.tadeas.ltt.menu.Button;
import me.tadeas.ltt.menu.MenuBuilder;
import me.tadeas.ltt.objects.HeadPlayer;
import me.tadeas.ltt.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.*;

/**
 * This file was created by Tadeáš Drab on 10. 5. 2020
 */
public class TrophyMenu extends MenuBuilder {

    private HashMap<Integer, List<ItemStack>> trophyPages = new HashMap<>();
    private HeadPlayer headPlayer;
    private int currentPage = 1;
    private int maxPage;
    private int maxItemsPerPage = 21;
    private boolean ownTrophies;

    public TrophyMenu(HeadTrophies plugin, HeadPlayer headPlayer, Player player, boolean ownTrophies) {
        super("§8§lYour Head Trophies", 54, plugin, player);

        this.ownTrophies = ownTrophies;
        this.headPlayer = headPlayer;

        Collection<ItemStack> itemStacks = headPlayer.getItemStackHeads();

        if (itemStacks.size() <= 0) {
            if (!this.ownTrophies)
                setName("§8§l" + headPlayer.getPlayer() + "'s Head Trophies");
            rebuildInventory();
            return;
        }

        maxPage = Math.round(itemStacks.size() / maxItemsPerPage) + 1;
        int page = 0;

        int currentCount = maxItemsPerPage + 1;
        Iterator<ItemStack> itemStackIterator = itemStacks.iterator();
        while (itemStackIterator.hasNext()) {
            ItemStack itemStack = itemStackIterator.next();

            if (currentCount > maxItemsPerPage) {
                page++;
                currentCount = 0;

                trophyPages.put(page, new ArrayList<>());
            }

            trophyPages.get(page).add(itemStack);
            currentCount++;
        }

        if (!this.ownTrophies)
            setName("§8§l" + headPlayer.getPlayer() + "'s Head Trophies - " + currentPage + "/" + maxPage);
        else
            setName("§8§lYour Head Trophies - " + currentPage + "/" + maxPage);

        rebuildInventory();
    }

    @Override
    public void buildInventory() {
        if (trophyPages.isEmpty()) {
            addItem(31, new ItemBuilder(Material.REDSTONE_BLOCK).setName("§cNo trophies where found :( ").toFinalStack());
            return;
        }

        int slot = 10;

        for (ItemStack itemStack : trophyPages.get(currentPage)) {
            addItem(slot, itemStack);
            slot++;

            if ((slot + 1) % 9 == 0)
                slot += 2;
        }

        if (currentPage < maxPage) {
            addButtonItem(51, new ItemBuilder(Material.ARROW).setName("§cGo to next page").toFinalStack(), new Button() {
                @Override
                public void onClickItem(InventoryClickEvent event) {
                    currentPage++;
                    if (!ownTrophies)
                        setName("§8§l" + headPlayer.getPlayer() + "'s Head Trophies - " + currentPage + "/" + maxPage);
                    else
                        setName("§8§lYour Head Trophies - " + currentPage + "/" + maxPage);
                    rebuildInventory();
                }
            });
        } else if (currentPage > 1) {
            addButtonItem(49, new ItemBuilder(Material.ARROW).setName("§cGo to previous page").toFinalStack(), new Button() {
                @Override
                public void onClickItem(InventoryClickEvent event) {
                    currentPage--;
                    if (!ownTrophies)
                        setName("§8§l" + headPlayer.getPlayer() + "'s Head Trophies - " + currentPage + "/" + maxPage);
                    else
                        setName("§8§lYour Head Trophies - " + currentPage + "/" + maxPage);
                    rebuildInventory();
                }
            });
        }
    }
}
