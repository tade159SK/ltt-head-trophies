package me.tadeas.ltt.objects;

import com.mojang.authlib.properties.Property;
import me.tadeas.ltt.HeadServerType;
import me.tadeas.ltt.HeadTrophies;
import me.tadeas.ltt.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

/**
 * This file was created by Tadeáš Drab on 10. 5. 2020
 */
public class HeadPlayer {

    private String player;
    private String uuid;
    private double percentHeadChance;
    private HashMap<String, ItemStack> playerHeads = new HashMap<>();
    private boolean privacy;

    public HeadPlayer(String player, String uuid, double percentHeadChance, boolean privacy) {
        this.player = player;
        this.uuid = uuid;
        this.percentHeadChance = percentHeadChance;
        this.privacy = privacy;
    }

    public String getPlayer() {
        return player;
    }

    public String getUuid() {
        return uuid;
    }

    public boolean isPrivacy() {
        return privacy;
    }

    public void setPrivacy(boolean privacy) {
        this.privacy = privacy;
    }

    public double getPercentHeadChance() {
        return percentHeadChance;
    }

    public void setPercentHeadChance(double percentHeadChance) {
        this.percentHeadChance = percentHeadChance;
    }

    public Collection<ItemStack> getItemStackHeads() {
        return playerHeads.values();
    }

    public ItemStack addPlayerHead(Player victim, HeadTrophies plugin) {
        Iterator<Property> propertyIterator = ((CraftPlayer) victim).getHandle().getProfile().getProperties().get("textures").iterator();
        while (propertyIterator.hasNext()) {
            Property property = propertyIterator.next();
            System.out.println("Found skin value for player " + victim.getName() + " uuid " + victim.getUniqueId().toString());
            if (playerHeads.containsKey(property.getValue())) {
                return null;
            }
            playerHeads.put(property.getValue(), new ItemBuilder(Material.SKULL_ITEM).setData((byte) 3).setName("§e§l" + victim.getName() + "'s Head").toSkullFinalStack(property.getValue()));

            if (plugin.getHeadServerType() != HeadServerType.DROP_ON_GROUND_ONLY)
                plugin.getDatabaseManager().addPlayerHead(getUuid(), victim.getName(), property.getValue());
            return playerHeads.get(property.getValue());
        }
        return null;
    }

    public void reAddPlayerHead(String victimName, String value) {
        playerHeads.put(value, new ItemBuilder(Material.SKULL_ITEM).setData((byte) 3).setName("§e§l" + victimName + "'s Head").toSkullFinalStack(value));
    }
}
