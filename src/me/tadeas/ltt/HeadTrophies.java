package me.tadeas.ltt;

import me.tadeas.ltt.commands.TrophyCommand;
import me.tadeas.ltt.manager.DatabaseManager;
import me.tadeas.ltt.manager.EventManager;
import me.tadeas.ltt.objects.HeadPlayer;
import me.tadeas.ltt.utils.Recharger;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.concurrent.ConcurrentHashMap;

/**
 * This file was created by Tadeáš Drab on 10. 5. 2020
 */
public class HeadTrophies extends JavaPlugin {

    private HeadServerType headServerType = HeadServerType.DROP_ON_GROUND_AND_TO_MENU;
    private DatabaseManager databaseManager;
    private Recharger recharger;
    private ConcurrentHashMap<Player, HeadPlayer> headPlayers = new ConcurrentHashMap<>();

    @Override
    public void onEnable() {
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        getCommand("trophies").setExecutor(new TrophyCommand(this));

        databaseManager = new DatabaseManager();
        new EventManager(this);
        recharger = new Recharger(this);
    }

    public HeadServerType getHeadServerType() {
        return headServerType;
    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    public Recharger getRecharger() {
        return recharger;
    }

    public ConcurrentHashMap<Player, HeadPlayer> getHeadPlayers() {
        return headPlayers;
    }

    public HeadPlayer getHeadPlayer(Player player) {
        return headPlayers.get(player);
    }
}
