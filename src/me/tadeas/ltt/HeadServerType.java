package me.tadeas.ltt;

/**
 * This file was created by Tadeáš Drab on 10. 5. 2020
 */
public enum HeadServerType {

    DROP_ON_GROUND_ONLY,
    DROP_ON_GROUND_AND_TO_MENU,
    ADD_TO_MENU
}
