package me.tadeas.ltt.manager;

import me.tadeas.ltt.HeadServerType;
import me.tadeas.ltt.HeadTrophies;
import me.tadeas.ltt.objects.HeadPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Random;

/**
 * This file was created by Tadeáš Drab on 12. 5. 2020
 */
public class EventManager implements Listener {

    private HeadTrophies plugin;
    private Random random = new Random();

    public EventManager(HeadTrophies plugin) {
        this.plugin = plugin;

        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        new BukkitRunnable() {
            @Override
            public void run() {
                if (!player.isOnline())
                    return;

                System.out.println("Downloading player data " + player.getName());
                HeadPlayer headPlayer = plugin.getDatabaseManager().getHeadPlayer(player);

                if (headPlayer != null) {
                    System.out.println("Downloading players heads data " + player.getName());
                    plugin.getDatabaseManager().getPlayerHeads(headPlayer);
                }

                plugin.getHeadPlayers().put(player, headPlayer);
            }
        }.runTaskLaterAsynchronously(plugin, 10);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        plugin.getHeadPlayers().remove(event.getPlayer());
        plugin.getRecharger().removeAllRecharge(event.getPlayer());
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Projectile || event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK) {
            event.getEntity().setMetadata("Heads-CD", new FixedMetadataValue(plugin, System.currentTimeMillis()));
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        Player killer = event.getEntity().getKiller();

        if (killer == null) {
            return;
        }

        Player victim = event.getEntity();

        if (victim.hasMetadata("Heads-CD")) {
            if (System.currentTimeMillis() - victim.getMetadata("Heads-CD").get(0).asLong() < 500) {
                return;
            }
        }

        Location victimLocation = victim.getLocation().add(0, 0.5, 0);
        HeadPlayer headPlayer = plugin.getHeadPlayer(killer);

        new BukkitRunnable() {
            @Override
            public void run() {
                double randdub = (random.nextDouble() * 100);
                double chance = (100.0d - headPlayer.getPercentHeadChance());

                if (randdub >= chance) {
                    ItemStack itemStack = headPlayer.addPlayerHead(victim, plugin);

                    if (plugin.getHeadServerType() != HeadServerType.ADD_TO_MENU && itemStack != null) {
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                victimLocation.getWorld().dropItem(victimLocation, itemStack);
                            }
                        }.runTask(plugin);
                    }

                    if (itemStack != null)
                        killer.sendMessage("§cYou have beheaded " + victim.getName() + "! Now he is your trophy!");
                }

                if (random.nextDouble() >= 0.99 && headPlayer.getPercentHeadChance() < 5) {
                    headPlayer.setPercentHeadChance(headPlayer.getPercentHeadChance() + 0.01);
                    plugin.getDatabaseManager().updatePlayerPercentChance(killer, headPlayer.getPercentHeadChance());
                }
            }
        }.runTaskAsynchronously(plugin);
    }
}
