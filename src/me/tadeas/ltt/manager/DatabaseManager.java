package me.tadeas.ltt.manager;

import me.tadeas.ltt.database.DataSource;
import me.tadeas.ltt.objects.HeadPlayer;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This file was created by Tadeáš Drab on 10. 5. 2020
 */
public class DatabaseManager {

    public HeadPlayer getHeadPlayer(Player player) {
        HeadPlayer headPlayer = hasHeadPlayer(player);

        if (headPlayer != null)
            return headPlayer;

        try (Connection connection = DataSource.getConnection()) {
            PreparedStatement getConfigStatement = connection.prepareStatement("insert into playerHeadsConfig (uuid, percentChance, lastName, privacy) VALUES (?, ?, ?, ?)");

            getConfigStatement.setString(1, player.getUniqueId().toString());
            getConfigStatement.setDouble(2, 0.5d);
            getConfigStatement.setString(3, player.getName().toLowerCase());
            getConfigStatement.setBoolean(4, true);

            getConfigStatement.execute();

            getConfigStatement.close();

            headPlayer = new HeadPlayer(player.getName(), player.getUniqueId().toString(), 1d, true);
            return headPlayer;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private HeadPlayer hasHeadPlayer(Player player) {
        try (Connection connection = DataSource.getConnection()) {
            PreparedStatement getConfigStatement = connection.prepareStatement("select * from playerHeadsConfig where uuid = ?");

            getConfigStatement.setString(1, player.getUniqueId().toString());

            HeadPlayer headPlayer = null;
            try (ResultSet resultSet = getConfigStatement.executeQuery()) {
                while (resultSet.next()) {
                    double percentChance = resultSet.getDouble("percentChance");
                    boolean privacy = resultSet.getBoolean("privacy");
                    headPlayer = new HeadPlayer(player.toString(), player.getUniqueId().toString(), percentChance, privacy);
                }
            }

            getConfigStatement.close();
            return headPlayer;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public HeadPlayer hasHeadPlayerByNickname(String player) {
        try (Connection connection = DataSource.getConnection()) {
            PreparedStatement getConfigStatement = connection.prepareStatement("select * from playerHeadsConfig where lastName = ?");

            getConfigStatement.setString(1, player);

            HeadPlayer headPlayer = null;
            try (ResultSet resultSet = getConfigStatement.executeQuery()) {
                while (resultSet.next()) {
                    double percentChance = resultSet.getDouble("percentChance");
                    boolean privacy = resultSet.getBoolean("privacy");
                    String uuid = resultSet.getString("uuid");
                    headPlayer = new HeadPlayer(player, uuid, percentChance, privacy);
                }
            }

            getConfigStatement.close();
            return headPlayer;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void getPlayerHeads(HeadPlayer headPlayer) {
        try (Connection connection = DataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("select * from playerHeads where uuid = ?");

            statement.setString(1, headPlayer.getUuid());

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    String victimName = resultSet.getString("victimName");
                    String value = resultSet.getString("victimSkin");
                    headPlayer.reAddPlayerHead(victimName, value);
                }
            }

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addPlayerHead(String uuid, String victimName, String value) {
        try (Connection connection = DataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO playerHeads (uuid, victimName, victimSkin) VALUES (?, ?, ?)");

            statement.setString(1, uuid);
            statement.setString(2, victimName);
            statement.setString(3, value);

            statement.execute();

            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updatePlayerPercentChance(Player player, double percentChance) {
        try (Connection connection = DataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("UPDATE playerHeadsConfig SET percentChance = ? WHERE uuid = ?");

            statement.setDouble(1, percentChance);
            statement.setString(2, player.getUniqueId().toString());

            statement.execute();

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updatePlayerPrivacy(Player player, boolean privacy) {
        try (Connection connection = DataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("UPDATE playerHeadsConfig SET privacy = ? WHERE uuid = ?");

            statement.setBoolean(1, privacy);
            statement.setString(2, player.getUniqueId().toString());

            statement.execute();

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
