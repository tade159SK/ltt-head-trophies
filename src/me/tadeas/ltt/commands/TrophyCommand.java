package me.tadeas.ltt.commands;

import me.tadeas.ltt.HeadTrophies;
import me.tadeas.ltt.menu.menus.TrophyMenu;
import me.tadeas.ltt.objects.HeadPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * This file was created by Tadeáš Drab on 12. 5. 2020
 */
public class TrophyCommand implements CommandExecutor {

    private HeadTrophies plugin;

    public TrophyCommand(HeadTrophies plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        if (!command.getName().equalsIgnoreCase("trophies"))
            return false;
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage("Sender must be a player!");
            return false;
        }
        Player player = (Player) commandSender;
        if (plugin.getRecharger().addToRecharge(player, 3000, "Trophies Command")) {
            player.sendMessage("§cSlow down using that command!");
            return false;
        }
        if (args.length == 0) {
            new TrophyMenu(plugin, plugin.getHeadPlayer(player), player, true);
            return true;
        } else if (args.length == 1 && args[0].equalsIgnoreCase("toggleprivacy")) {
            plugin.getHeadPlayer(player).setPrivacy(!plugin.getHeadPlayer(player).isPrivacy());

            if (plugin.getHeadPlayer(player).isPrivacy()) {
                player.sendMessage("§aYour trophies were set to private mode!");
            } else {
                player.sendMessage("§aYour trophies were set to public mode!");
            }

            new BukkitRunnable() {
                @Override
                public void run() {
                    plugin.getDatabaseManager().updatePlayerPrivacy(player, plugin.getHeadPlayer(player).isPrivacy());
                }
            }.runTaskAsynchronously(plugin);
            return true;
        } else if (args.length >= 0) {
            String toFindPlayer = args[0];

            new BukkitRunnable() {
                @Override
                public void run() {
                    HeadPlayer headPlayer = plugin.getDatabaseManager().hasHeadPlayerByNickname(toFindPlayer.toLowerCase());

                    if (headPlayer == null) {
                        player.sendMessage("§cPlayer '" + toFindPlayer + "' does not exists!");
                        return;
                    }
                    if (headPlayer.isPrivacy()) {
                        player.sendMessage("§cPlayer '" + toFindPlayer + "' has his trophies in private mode!");
                        return;
                    }
                    plugin.getDatabaseManager().getPlayerHeads(headPlayer);

                    new TrophyMenu(plugin, headPlayer, player, false);
                }
            }.runTaskAsynchronously(plugin);
        }
        return false;
    }
}
